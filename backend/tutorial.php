<?php
header('Access-Control-Allow-Origin: *');
/* Подключение к серверу MySQL */
$link = mysqli_connect(
    'std-mysql', /* Хост, к которому мы подключаемся */
    'std_228', /* Имя пользователя */
    'RMN013mr', /* Используемый пароль */
    'std_228'); /* База данных для запросов по умолчанию */
if (!$link) { printf("Невозможно подключиться к базе данных. Код ошибки: %s\n", mysqli_connect_error()); exit; }

$result = mysqli_query($link, 'SELECT `text`, `show_btn` FROM `tutorial`');

$array = [];
$i = 0;
while( $row = mysqli_fetch_array($result) ){
    $array[$i] = ["text"=> $row['text'], "show" => $row['show_btn']];
    $i++;
}

$data = ['tutorial' => $array];
echo json_encode($data);