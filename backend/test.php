<?php
header('Access-Control-Allow-Origin: *');
/* Подключение к серверу MySQL */
$link = mysqli_connect(
    'std-mysql', /* Хост, к которому мы подключаемся */
    'std_228', /* Имя пользователя */
    'RMN013mr', /* Используемый пароль */
    'std_228'); /* База данных для запросов по умолчанию */
if (!$link) { printf("Невозможно подключиться к базе данных. Код ошибки: %s\n", mysqli_connect_error()); exit; }

$result = mysqli_query($link, 'SELECT `id`, `text`, `id_answers` FROM `test`');

$array = [];
$i = 0;
while( $row = mysqli_fetch_array($result) ){
    $answers_arr = [];
    $j = 0;
    $answers = mysqli_query($link, "SELECT `answerText`, `specialnost`, `value` FROM `answers` WHERE `id_test` ="."'".$row['id']."'");
    while( $row_ans = mysqli_fetch_array($answers) ){
        $answers_arr[$j] = ['answerText'=> $row_ans['answerText'], 'specialnost' => $row_ans['specialnost'], 'value'=> $row_ans['value']];
        $j++;

    }
    $array[$i] = ['text' => $row['text'], 'answers' => $answers_arr];
    $i++;
}

$data = ['test' => $array];
echo json_encode($data);