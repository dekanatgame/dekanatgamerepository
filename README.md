# Игра "Деканат"
Дизайн: <https://www.figma.com/file/EpQ804M53lOgdJ16ydjSwC2L/gameDekanat?node-id=0%3A1>
## Как запустить клиентскую часть:
 - Устанавливаем node.js <https://nodejs.org/>
 - Устанавливаем Angular CLI 
```ssh
$ npm install -g @angular/cli
``` 
 - Клонируем репозиторий 
 - Устанавливаем зависимости для проекта, предварительно зайдя в папку проекта ../dekanatGame
```ssh
$ npm install
``` 
  - Запускаем проект
```ssh
$ ng serve
``` 