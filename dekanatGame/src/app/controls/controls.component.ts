///<reference path="../../../node_modules/@angular/core/src/metadata/ng_module.d.ts"/>
import {Component, EventEmitter, Input, NgModule, OnInit, Output} from '@angular/core';
import {MainService} from "../main.service";


@Component({
  selector: 'app-controls',
  templateUrl: './controls.component.html',
  styleUrls: ['./controls.component.css']
})

export class ControlsComponent implements OnInit {
  public sentBtn;
  public setting_btn;
  public popUp = false;
  public popUpStatus = true;
  @Input() animate: string;
  @Input() stopAnimation: string;
  @Input() edu_procent;
  @Input() life_procent;
  @Input() social_procent;//
  @Output() valueChange = new EventEmitter <boolean>();
  @Output() change: EventEmitter<any> = new EventEmitter();
  @Output() action: EventEmitter<any> = new EventEmitter();
  @Output() nextWeek = new EventEmitter();
  @Input() answers;
  @Input() game_btn;
  @Input() enable;
  public logical1: boolean = this.animate !== "question-btn" && this.animate !== undefined && this.answers === undefined;
  public logical2 = this.animate === "question-btn" || this.answers !== undefined;
  @Output() newBar = new EventEmitter<boolean>();
  public audio_img = "assets/imgs/sound_off.png";
  @Output() changeAudioStatus: EventEmitter<any> = new EventEmitter();
  public rule:boolean = false;
  public ruleText: string;

  constructor(private mainService: MainService) { }

  ngOnInit() {
    this.ruleText = this.mainService.ruletext;
  }

  ngDoCheck(){
    this.logical1 = this.animate !== "question-btn" && this.animate !== undefined && this.answers === undefined || this.game_btn;
    this.logical2 = this.animate === "question-btn" || this.answers !== undefined;


    switch (this.animate){
      case "a_btn": this.sentBtn = "a_btn";
        break;
      case "b_btn": this.sentBtn = "b_btn";
        break;
      case "c_btn": this.sentBtn = "c_btn";
        break;
      case "r_arrow_btn": this.sentBtn = "r_arrow_btn";
        break;
      case "setting_btn": this.setting_btn = true;
        break;
      case "edu": this.sentBtn = "edu";
        break;
      case "life": this.sentBtn = "life";
        break;
      case "social": this.sentBtn = "social";
        break;
      case "question-btn": this.sentBtn = "question-btn";
        break;
    }

    switch (this.stopAnimation){
      case "a_btn": this.stopAnimation = "a_btn";
        break;
      case "b_btn": this.stopAnimation= "b_btn";
        break;
      case "c_btn": this.stopAnimation = "c_btn";
        break;
      case "r_arrow_btn": this.stopAnimation = "r_arrow_btn";
        break;
      case "setting_btn": this.setting_btn = false;
        break;
      case "edu": this.stopAnimation = "edu";
        break;
      case "life": this.stopAnimation = "life";
        break;
      case "social": this.stopAnimation = "social";
        break;
      case "question-btn": this.stopAnimation = "question-btn";
        break;
    }

  }

  onChange(){
    this.change.emit("test");
  }

  onAction(){
    this.action.emit();
  }

  onNewBar(){
    this.newBar.emit();
  }

  exit(){
      this.mainService.setCurrentPage('start');
      this.change.emit("start");
      this.popUp = false;
      this.mainService.clearMessange();
  }

  openPopUp(){
    if(this.popUpStatus){
      this.popUp = !this.popUp;
      this.rule = false;
    }


  }

  onNextWeek(){
    this.nextWeek.emit()
  }
  audio(){
      this.popUpStatus = !this.popUpStatus;
      let status = this.mainService.get_audioStatus();
      this.audio_img = status ? "assets/imgs/sound_on.png" : "assets/imgs/sound_off.png";
      this.mainService.change_audioStatus();
      this.changeAudioStatus.emit();
      setTimeout(()=>{
        this.popUpStatus = !this.popUpStatus;
      },1000);


  }
  openRule(){
    this.popUpStatus = !this.popUpStatus;
    this.rule = !this.rule;


    setTimeout(()=>{
      this.popUpStatus = !this.popUpStatus;
    },1000);
  }

}
