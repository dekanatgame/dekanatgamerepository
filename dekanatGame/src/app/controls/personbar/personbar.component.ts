import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-personbar',
  templateUrl: './personbar.component.html',
  styleUrls: ['./personbar.component.css']
})
export class PersonbarComponent implements OnInit {
  @Input() edu_procent;
  @Input() life_procent;
  @Input() social_procent;
  @Input() animate;
  public edu_animation: boolean;
  public life_animation: boolean;
  public social_animation: boolean;
  @Input() stopeAnimation;

  constructor() { }

  ngOnInit() {

  }

  ngDoCheck(){
    if(this.edu_procent == undefined){
      this.edu_procent = 75;
    }
    if(this.life_procent == undefined){
      this.life_procent = 23;
    }
    if(this.social_procent == undefined){
      this.social_procent = 53;
    }
    switch(this.animate){
      case 'edu': this.edu_animation = true;
      break;
      case 'life': this.life_animation = true;
      break;
      case 'social': this.social_animation = true;
        break;
    }

    switch(this.stopeAnimation){
      case 'edu': this.edu_animation = false;
        break;
      case 'life': this.life_animation = false;
        break;
      case 'social': this.social_animation = false;
        break;
    }
  }

}
