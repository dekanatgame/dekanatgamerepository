import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MainService} from "../../main.service";

@Component({
  selector: 'app-questions-btns',
  templateUrl: './questions-btns.component.html',
  styleUrls: ['./questions-btns.component.css']
})
export class QuestionsBtnsComponent implements OnInit {
  public doAnimate;
  @Input() animate: string;
  @Input() stopeAnimation: string;
  @Input() answers;
  //public enable: boolean = true;
  @Output() action: EventEmitter<any> = new EventEmitter();
  @Input() enable;
  constructor(private mainService: MainService) { }

  ngOnInit() {

  }

  ngDoCheck(){
    switch(this.animate){
      case "question-btn": this.doAnimate = true;
    }
    switch(this.stopeAnimation){
      case "question-btn": this.doAnimate = false;
    }
  }

  click(value, conclusionText, enable) {
    if (enable && this.mainService.getCurrentPage() == "main"){
      this.enable = false;
      this.mainService.setProcentBar(value);
    this.mainService.clearMessange();
    let timer = 0; //Задержка вывода первой буквы
    let delay = 100; // Задержка вывода каждой последующей буквы в мс
    for (let _j = 0; _j < conclusionText.length; _j++) {
      this.mainService.mainTimeout = setTimeout(() => {
        this.mainService.setMessange(conclusionText[_j])
      }, timer);
      timer += delay;
      if (_j === 0) {
        setTimeout(() => {

          this.action.emit();
          this.enable = true;
        }, conclusionText.length * delay + (timer - delay) + 1); //1 Задержка в конце сообщения
      }
    }
    }
  }

}
