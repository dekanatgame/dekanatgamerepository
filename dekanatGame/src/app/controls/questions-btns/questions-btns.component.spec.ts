import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionsBtnsComponent } from './questions-btns.component';

describe('QuestionsBtnsComponent', () => {
  let component: QuestionsBtnsComponent;
  let fixture: ComponentFixture<QuestionsBtnsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestionsBtnsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionsBtnsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
