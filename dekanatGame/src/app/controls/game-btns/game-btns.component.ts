import {Component, OnInit, Input, EventEmitter, Output, NgModule} from '@angular/core';
import {MainService} from "../../main.service";

@Component({
  selector: 'app-game-btns',
  templateUrl: './game-btns.component.html',
  styleUrls: ['./game-btns.component.css']
})

@NgModule({
  providers: [MainService]
})


export class GameBtnsComponent implements OnInit {
  public a_btn;
  public b_btn;
  public c_btn;
  public r_arrow_btn;
  @Input() stopAnimation: string;
  @Input() animatingBtn: string;
  @Output() valueChange = new EventEmitter <boolean>();
  @Output() change: EventEmitter<any> = new EventEmitter();
  @Output() click = new EventEmitter<boolean>();
  @Output() newBar = new EventEmitter<boolean>();
  @Output() nextWeek = new EventEmitter();
  private currentPage;
  constructor(private mainService: MainService) { }

  ngOnInit() {
    this.currentPage = this.mainService.getCurrentPage();
  }
  ngDoCheck(){
    switch (this.animatingBtn){
      case "a_btn": this.a_btn = true;
        break;
      case "b_btn": this.b_btn = true;
        break;
      case "c_btn": this.c_btn = true;
        break;
      case "r_arrow_btn": this.r_arrow_btn = true;
        break;
    }

    switch (this.stopAnimation){
      case "a_btn": this.a_btn = false;
        break;
      case "b_btn": this.b_btn = false;
        break;
      case "c_btn": this.c_btn = false;
        break;
      case "r_arrow_btn": this.r_arrow_btn = false;
        break;
    }
  }

  nextPage(){
    if(this.currentPage == "tutorial"){
      this.mainService.setCurrentPage("test");
      this.change.emit("test");
    }

    if(this.currentPage == "main"){
      //this.newBar.emit();
      this.nextWeek.emit();
    }
  }

  a_btn_click(){
    if(this.currentPage == "main"){
      if(this.mainService.social_procent + 5 > 100){
        this.mainService.social_procent = 100;
      }else{
        this.mainService.social_procent += 5
      }
      this.nextWeek.emit();
    }
  }

  b_btn_click(){
    if(this.currentPage == "main"){
      if(this.mainService.edu_procent + 10 > 100){
        this.mainService.edu_procent = 100;
      }else{
        this.mainService.edu_procent += 10
      }
      this.nextWeek.emit();
    }
  }

  c_btn_click(){
    if(this.currentPage == "main"){
      if(this.mainService.life_procent + 5 > 100){
        this.mainService.life_procent = 100;
      }else{
        this.mainService.life_procent += 5
      }
      this.nextWeek.emit();
    }
  }
}
