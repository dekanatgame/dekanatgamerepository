import {Component, NgModule} from '@angular/core';
import {MainService} from "./main.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
@NgModule({
  providers: [MainService]
})

export class AppComponent {
  title = 'dekanatGame';
  currentPage: string;
  private audio;
  constructor(private mainService: MainService){}

  ngOnInit() {
    this.currentPage = this.mainService.setCurrentPage("start");
    //setTimeout(()=>{this.currentPage = this.mainService.setCurrentPage("gameover");},1000);
    this.mainService.reqTutorialData();
    this.mainService.reqTestData();
    //this.mainService.reqMainlData();

    this.audio = new Audio();
    this.audio.src = "assets/audio/Sneaky_Business.mp3";
    this.audio.load();
    this.audio.volume = 0.1;

    if(this.mainService.get_audioStatus()){
      this.audio.play();
    }
  }

  onChange(){
    this.currentPage = this.mainService.setCurrentPage(this.mainService.getCurrentPage());
  }

  onChangeAudioStatus(){
    if(this.mainService.get_audioStatus()){
      this.audio.pause();
    }else{
      this.audio.play();
    }
  }
}
