import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { StartComponent } from './start/start.component';
import { TutorialComponent } from './tutorial/tutorial.component';
import { HttpClientModule }   from '@angular/common/http';
import { ControlsComponent } from './controls/controls.component';
import { GameBtnsComponent } from './controls/game-btns/game-btns.component';
import { QuestionsBtnsComponent } from './controls/questions-btns/questions-btns.component';
import { PersonbarComponent } from './controls/personbar/personbar.component';
import { TestComponent } from './test/test.component';
import { MainComponent } from './main/main.component';
import { SessionComponent } from './session/session.component';
import { GameoverComponent } from './gameover/gameover.component';
import { WinComponent } from './win/win.component';

@NgModule({
  declarations: [
    AppComponent,
    StartComponent,
    TutorialComponent,
    ControlsComponent,
    GameBtnsComponent,
    QuestionsBtnsComponent,
    PersonbarComponent,
    TestComponent,
    MainComponent,
    SessionComponent,
    GameoverComponent,
    WinComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
