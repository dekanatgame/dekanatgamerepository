import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {MainService} from "../main.service";

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {
  data;
  currentEx = 0;
  lastEx;
  answers;
  public game_btn = false;
  @Output() change: EventEmitter<any> = new EventEmitter();
  @Output() changeAudioStatus: EventEmitter<any> = new EventEmitter();

  constructor(private mainService: MainService) { }

  ngOnInit() {
    this.data = this.mainService.getTestData().test;
    this.lastEx = this.data.length;
    this.answers = this.data[this.currentEx].answers;
  }

  click(answer){
    if(this.currentEx !== this.lastEx-1){
      this.mainService.addOps(answer.specialnost, answer.value); //Добавляем в общий стор баллы за ответы
      this.currentEx++;
    } else if(this.currentEx === this.lastEx-1){
      this.mainService.addOps(answer.specialnost, answer.value);
      let op = this.mainService.getOps();
      let max = 0;
      let yourOp;
      for(let key in op){
        if(max < op[key]) {
          max = op[key];
          yourOp = key;
        }
      }
      this.mainService.setYourOp(yourOp);
      this.mainService.reqMainlData();
      this.mainService.reqSessionlData();
      setTimeout(()=>{
        this.mainService.setCurrentPage('main');
        this.change.emit("main");
      },500)

    }
  }
  ngDoCheck(){
    this.data = this.mainService.getTestData().test;
    this.lastEx = this.data.length;
    this.answers = this.data[this.currentEx].answers;
  }

  onChange(){
    this.change.emit();
  }

  onChangeAudioStatus($event){
    this.changeAudioStatus.emit();
  }
}
