import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MainService} from "../main.service";

@Component({
  selector: 'app-session',
  templateUrl: './session.component.html',
  styleUrls: ['./session.component.css']
})
export class SessionComponent implements OnInit {
  public data;
  currentEx = 0;
  lastEx;
  answers = undefined;
  game_btn = false;
  life_procent;
  edu_procent;
  social_procent;
  @Output() change: EventEmitter<any> = new EventEmitter();
  @Output() changeAudioStatus: EventEmitter<any> = new EventEmitter();

  constructor(private mainService: MainService) { }

  ngOnInit() {
    this.life_procent = this.mainService.life_procent;
    this.edu_procent = this.mainService.edu_procent;
    this.social_procent = this.mainService.social_procent;

    this.data = this.mainService.getSessionData().session;
    this.lastEx = this.data.length;
    this.answers = this.data[this.currentEx].answers;
  }

  click(answer){
    if(this.currentEx !== this.lastEx-1){
      this.mainService.setProcentBar(answer.value);
      this.currentEx++;
    } else if(this.currentEx === this.lastEx-1){
      this.mainService.setProcentBar(answer.value);
      //
      if(this.mainService.life_procent > 0 && this.mainService.edu_procent > 0 && this.mainService.social_procent > 0 ){
        this.mainService.setCurrentPage('win');
        this.change.emit('win');
      }else{
        this.mainService.setCurrentPage('gameover');
        this.change.emit('gameover');
      }


    }
  }

  ngDoCheck(){
    this.life_procent = this.mainService.life_procent;
    this.edu_procent = this.mainService.edu_procent;
    this.social_procent = this.mainService.social_procent;

    // if(this.mainService.getCurrentPage() == "gameover"){
    //   this.change.emit('gameover');
    // }

    this.data = this.mainService.getSessionData().session;
    this.lastEx = this.data.length;
    this.answers = this.data[this.currentEx].answers;
  }
  onChange(){
    this.change.emit();
  }
  onChangeAudioStatus($event){
    this.changeAudioStatus.emit();
  }

}
