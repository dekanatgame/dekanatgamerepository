import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {MainService} from "../main.service";

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  private data;
  private yourOp;
  private background: string = "assets/imgs/bg_electro.png";
  public character: string;
  private testConclusion: string;
  public messange: string ='';
  private w: number = 0;
  private d: number =0;
  private days;
  public answers;
  public game_btn: boolean = false;
  public life_procent;
  public edu_procent;
  public social_procent;
  public enable = false;

  @Output() change: EventEmitter<any> = new EventEmitter();
  @Output() changeAudioStatus = new EventEmitter();

  constructor(private mainService: MainService) { }

  ngOnInit() {
    this.life_procent = this.mainService.life_procent;
    this.edu_procent = this.mainService.edu_procent;
    this.social_procent = this.mainService.social_procent;

    this.data = this.mainService.getMainData().weeks.sort((a,b)=> a.prioritet - b.prioritet);
    this.yourOp = this.mainService.getYourOp();
    this.character = "assets/imgs/curator.png";
    this.testConclusion = `Вам больше всего подходит специальность ${this.yourOp}`; //Добавить сообщение

    let timer = 0; //Задержка вывода первой буквы
    let delay = 100; // Задержка вывода каждой последующей буквы в мс
      for(let _i = 0; _i < this.testConclusion.length; _i++){
        setTimeout(()=>{this.mainService.setMessange(this.testConclusion[_i])}, timer);
        timer += delay;
        if(_i === 0) {
          setTimeout(() => {
            this.mainService.clearMessange();
            /****************************************/
            this.func();
            /****************************************/
          }, this.testConclusion.length * delay + (timer-delay) + 1000); // 1 - Задержка в конце сообщения
        }
      }

  }

  ngDoCheck(){
    this.messange = this.mainService.getMessange();
    this.life_procent = this.mainService.life_procent;
    this.edu_procent = this.mainService.edu_procent;
    this.social_procent = this.mainService.social_procent;
  }
  onAction(){
    console.log(this.data);
    if(this.d < this.data[this.w].days.length-1){
      this.d++;
      console.log("Work2"); // Вызывается после каждого задания
      if(this.days[this.d].answers.length !== 0){
        console.log("wtf");
        this.enable = false;
        this.func();
      }else if(this.data[this.w].days[this.d].introductionText == "") {
        console.log("Пустой текст");
        this.onAction();
      }else{
        console.log("null");
        this.onAction();
      }

    }else if(this.d == this.data[this.w].days.length-1 && this.w < this.data.length-1){
      this.w++;
      console.log(this.data[this.w].days);
      this.d = 0;
      if(this.data[this.w].days.length == 0){
        console.log("err");
        this.onAction();
      }else
      if(this.data[this.w].days[this.d].answers == undefined){
        console.log("null Ex2222");
        this.onAction();
      }else if(this.data[this.w].days[this.d].answers.length === 0){
        console.log("null Ex2222 222 222 22");
        this.onAction();
      }else if(this.data[this.w].days[this.d].introductionText == ""){
          console.log("Пустой текст");
          this.onAction();

      }else{
        console.log("Work3"); // Вызывается в конце недели
        this.enable = false;
        this.answers = undefined;
        this.game_btn = true;
      }


      if(this.data[this.w].days[0] === undefined){
        console.log("Next week is undefined");
        this.mainService.setCurrentPage("session");
        this.change.emit("session");
      }




    }else if(this.d == this.data[this.w].days.length-1 && this.w == this.data.length-1){
      this.answers = undefined;
      console.log("Work4"); // Вызывается перед сессией
      this.mainService.setCurrentPage("session");
      this.change.emit("session");

    }else{
      this.answers = undefined;
      console.log("Work5"); // Вызывается в случае ошибки
      this.mainService.setCurrentPage("session");
      this.change.emit("session");
    }
  }

  func(){
    this.days = this.data[this.w].days.sort((a,b)=> a.prioritet - b.prioritet);
    this.background = this.days[this.d].background;
    this.character = this.days[this.d].character;
    this.answers = this.days[this.d].answers;

    let timer = 0; //Задержка вывода первой буквы
    let delay = 100; // Задержка вывода каждой последующей буквы в мс
    this.mainService.clearMessange();
    for(let _j = 0; _j < this.days[this.d].introductionText.length; _j++){
      this.mainService.mainTimeout = setTimeout(()=>{this.mainService.setMessange(this.days[this.d].introductionText[_j])}, timer);
      timer += delay;
      if(_j === 0) {
        setTimeout(() => {
          this.enable = true;

        }, this.days[this.d].introductionText.length * delay + (timer-delay) + 1); //1 Задержка в конце сообщения
      }
    }
  }

  onNewBar(){
    console.log("My gosh");
    this.onAction();
  }

  onChange(){
    this.change.emit("test");
  }

  onNextWeek(){
    this.game_btn = false;
    this.func();
  }
onChangeAudioStatus(){
  this.changeAudioStatus.emit();
}
}
