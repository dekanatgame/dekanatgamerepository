import {Component, EventEmitter, NgModule, OnInit, Output} from '@angular/core';
import {MainService} from "../main.service";

@Component({
  selector: 'app-start',
  templateUrl: './start.component.html',
  styleUrls: ['./start.component.css']
})

@NgModule({
  providers: [MainService]
})

export class StartComponent implements OnInit {
  @Output() click = new EventEmitter<boolean>();
  public audio_img = "assets/imgs/sound_off.png";

  public rule:boolean = false;
  public popUp = false;
  private popUpStatus = true;
  public ruleText;

  constructor(private mainService: MainService) { }

  ngOnInit() {
    this.ruleText = this.mainService.ruletext;
  }

  @Output() change: EventEmitter<any> = new EventEmitter();
  @Output() changeAudioStatus: EventEmitter<any> = new EventEmitter();

  begin() {
     this.mainService.setCurrentPage('tutorial');
     this.change.emit("tutorial");
    }

    audio(){
    let status = this.mainService.get_audioStatus();
    this.audio_img = status ? "assets/imgs/sound_on.png" : "assets/imgs/sound_off.png";
    this.mainService.change_audioStatus();
    this.changeAudioStatus.emit();
    }

  openPopUp() {
    if (this.popUpStatus) {
      this.popUp = !this.popUp;
      this.rule = false;
    }
  }

  openRule(){
    this.openPopUp();
    this.popUpStatus = !this.popUpStatus;
    this.rule = !this.rule;


    setTimeout(()=>{
      this.popUpStatus = !this.popUpStatus;
    },1000);
  }
}
