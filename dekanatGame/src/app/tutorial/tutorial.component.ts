import {Component, EventEmitter, Injectable, NgModule, OnInit, Output} from '@angular/core';
import {MainService} from "../main.service";

@Component({
  selector: 'app-tutorial',
  templateUrl: './tutorial.component.html',
  styleUrls: ['./tutorial.component.css']
})

@NgModule({
  providers: [MainService]
})

@Injectable()
export class TutorialComponent implements OnInit {
  public data;
  public messange = '';
  public animate;
  public stopAnimation;
  @Output() change: EventEmitter<any> = new EventEmitter();
  @Output() changeAudioStatus: EventEmitter<any> = new EventEmitter();

  constructor(private mainService: MainService) {

  }

  ngOnInit() {
    this.data = this.mainService.getTutorialData();
    this.tellStory();


  }

  tellStory(){
    let timer = 0; //Задержка вывода первой буквы
    let delay = 100; // Задержка вывода каждой последующей буквы в мс
    for (let _i = 0; _i < this.data.tutorial.length; _i++) {
      setTimeout(() => {
        this.animate = this.data.tutorial[_i].show;
      }, timer); // Добавляем Анимацию, если есть
      for(let _j = 0; _j < this.data.tutorial[_i].text.length; _j++){
        var writeMess = setTimeout(()=>{this.messange += this.data.tutorial[_i].text[_j]}, timer);
        timer += delay;
        if(_j === 0) {
          var deleteMess = setTimeout(() => {
            this.messange = "";
            this.stopAnimation = this.data.tutorial[_i].show;
          }, this.data.tutorial[_i].text.length * delay + (timer-delay));
        }
      }
    }
  }

  onChange(){
    this.change.emit('test');
  }

onChangeAudioStatus(){
    this.changeAudioStatus.emit();
}

}
