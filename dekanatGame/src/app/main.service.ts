import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class MainService {
  currentPage: string;
  tutorialData;
  testData;
  private mainData;
  private ops: object = {};
  private yourOp: string;
  public edu_procent = 100;
  public life_procent = 100;
  public social_procent = 100;
  private messange:string="";
  private audio_status: boolean = false;//
  private sessionData;
  public mainTimeout;
  public ruletext = "Чтобы успешно закончить институт, необходимо уследить за всеми шкалами, верно решить поставленные задачи и закрыть сессию. В правом верхнем углу есть три шкалы: Учеба, Энергия, Отношения с группой.\n" +
    "Изначально до сессии остается 16 недель, во время каждой из них вы можете выполнить 5 действий. В зависимости от их выполнения размер шкал будет изменяться.\n" +
    "Участвуйте в ДОДах, помогайте деканату, правильно решайте проблемы в своей группе, отвечайте на вопросы и тогда сессия пройдет для вас незаметно. Если вдруг какая-либо шкала, например, энергии близка к нулю, стоит позаботиться о своем состоянии.\n" +
    "Нажимай на кнопку \"Перехода\", чтобы начать новую неделю.\n" +
    "Удачной игры :3";

  constructor(private http: HttpClient) { }

  getCurrentPage(){
    return this.currentPage;
  }
  setCurrentPage(page){
    this.currentPage = page;
    return this.currentPage;
  }

  reqTutorialData(){
    this.http.get('http://api-pd.std-228.ist.mospolytech.ru/tutorial.php').subscribe(data => {this.tutorialData = data});
  }
  getTutorialData(){
    return this.tutorialData;
  }
  reqTestData(){
    this.http.get('http://api-pd.std-228.ist.mospolytech.ru/test.php').subscribe(data => {this.testData = data;});
  }
  getTestData(){
    return this.testData;
  }

  addOps(name, value){
    if(this.ops[name] == undefined){
      this.ops[name] = value;
    }else{
      this.ops[name] += value;
    }
  }
  getOps(){
    return this.ops;
  }
  setYourOp(op){
    this.yourOp = op;
  }
  getYourOp(){
    return this.yourOp;
  }

  reqMainlData(){
    this.http.get(`http://api-pd.std-228.ist.mospolytech.ru/history.php?scpecialnost="${this.yourOp}"`).subscribe(data => {this.mainData = data});
    //this.http.get("assets/history.json").subscribe(data => {this.mainData = data});
  }
  getMainData(){
    return this.mainData;
  }
  //

  reqSessionlData(){
    this.http.get(`http://api-pd.std-228.ist.mospolytech.ru/session.php?scpecialnost="${this.yourOp}"`).subscribe(data => {this.sessionData = data});
  }
  getSessionData(){
    return this.sessionData;
  }

  getEduProcent(){
    return this.edu_procent;
  }
  setEduProcent(num, bool){
    if(bool){
      this.edu_procent += num;
    }else{
      this.edu_procent = 100;
    }
  }

  setLifeProcent(num, bool){
    if(bool){
      this.life_procent += num;
    }else{
      this.life_procent = 100;
    }
  }

  setSocialProcent(num, bool){
    if(bool){
      this.social_procent += num;
    }else{
      this.social_procent = 100;
    }
  }


  setProcentBar(value){
    if(this.social_procent + value['social'] >= 100){
      this.setSocialProcent(value['social'], false)
    }
    if(this.social_procent + value['social'] < 100 && value['social']!== undefined){
      this.setSocialProcent(value['social'], true)
    }
    if(this.edu_procent + value['education'] >= 100){
      this.setEduProcent(value['education'], false)
    }
    if(this.edu_procent + value['education'] < 100 && value['education'] !== undefined){
      this.setEduProcent(value['education'], true)
    }
    if(this.life_procent + value['life'] >= 100){
      this.setLifeProcent(value['life'], false)
    }
    if(this.life_procent + value['life'] < 100 && value['life']!== undefined){
      this.setLifeProcent(value['life'], true);
    }
  }

  setMessange(str){
    this.messange += str;
  }
  getMessange(){
    return this.messange;
  }
  clearMessange(){
    this.messange="";
  }
  change_audioStatus(){
    this.audio_status = !this.audio_status;
    return this.audio_status;
  }

  get_audioStatus(){
    return this.audio_status;
  }
}
